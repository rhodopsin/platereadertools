# -*- coding: utf-8 -*-
"""
Created on Sun Jul  2 15:57:33 2017

@author: hugeneuron
hugeneuron@gmail.com
"""
import glob
import os
import re


def getFileList():
    os.chdir("input")
    fileList= []
    trait = r"^Data \d+-\d+-\d+-\d{6}.txt$"

    for file in glob.glob("*.txt"):         #get all txt files
        if re.findall(trait,file) != "":    #check format of filename
            fileList.append(file)
    return fileList

if __name__ == "__main__":
    print(getFileList())