# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 18:02:55 2017

@author: neuron
"""

## calculate the average of two readings of the same plate, and generate a new CSV format spread sheet
import txt_to_csv
import os
import numpy as np
import listfiles


def plate_average(): # inputs are absorbance data in array form
    filelist = listfiles.getFileList()
    if len(filelist) == 2:
        plate1 = txt_to_csv.convert(filelist[0])[2]
        plate2 = txt_to_csv.convert(filelist[1])[2]
        plate12 = [(a + b)/2 for a, b in zip(plate1, plate2)]
        plate12 = txt_to_csv.format_data(plate12)
        os.chdir("../output")
        np.savetxt("averaged_plate.csv", plate12, delimiter=",", fmt='%s')
        return plate12
    elif len(filelist) > 2:
        print("more than 2 files in input folder")
    else:
        print("less than 2 files in input folder")
        
if __name__ == "__main__":
    print(plate_average())