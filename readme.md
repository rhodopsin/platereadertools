This script provides data export from Molecular devices plate readers.
The original exported txt file is horrible for readablity, so I created this script to make it neat.

You need python3 to run the script. You can download it here:
https://www.python.org/downloads/

For the script to run properly, don't mess with the folder structure.
Put your raw txt file in "input" folder, double click on "txt_to_csv.py". If you are asked which program do you want to use for opening it, find the "python.exe" in your python installation directory, then you can find your csv file in the "output" folder. If you have multiple txt files, you don't need to operate on each separately, just put all of the raw txt files in "input" folder, and proceed.

In our lab, we scan the plate for twice as technical duplicates, because we want to reduce the mechanical error caused by the machine.
If you scan the plate twice like we do, put only those two files in "input" folder, and double click on "plate_average.py"

If you have any questions or suggestions about this script, please send email to hugeneuron@gmail.com