import numpy as np
import listfiles
import os

def format_raw(opened): # extract useful lines from opened file.
    loaded = opened.readlines()
    useful = loaded[2:7]
    meta = useful[1]
    data = useful[3]
    return {"meta": meta, "data": data}


def extract_meta(meta_thread): # extract useful info from the line containing metadata.
    parsed_meta = meta_thread.split("\t")
    absorbance = parsed_meta[15]
    plate_format = parsed_meta[18]
    return {"absorbance": absorbance, "plate_format": plate_format}

    
def extract_data(data_thread): # extract absorbance data from the line containing data
    parsed_data = data_thread.split("\t")
    temperature = parsed_data[1]
    reading = parsed_data[2:-1]
    floatedreading = []
    for i in reading:
        floatedreading.append(float(i))
    return {"temp": temperature, "reading": floatedreading}


def format_data(ready_data):# format data into 8*12 array
    count_unit = len(ready_data)
    if count_unit == 96:
        pass
    else:
        print ("data incomplete/corrupt, check the exported txt file")
    lines = []
    starts = range(0, 96, 12)
    ends = [x + 12 for x in starts]
    for i in range(8):
        lines.append(ready_data[starts[i]:ends[i]])
    return lines


def export_csv(formated_data, outname): #export formatted data to csv file
    np.savetxt(outname[5:-4]+".csv", formated_data, delimiter=",", fmt='%s')

    
def convert(filename): #streamline the process
    raw_file = open(filename, "r", encoding = "latin-1")
    raw_formated = format_raw(raw_file)
    meta_extracted = extract_meta(raw_formated["meta"])
    data_extracted = extract_data(raw_formated["data"])
    data_formated = format_data(data_extracted["reading"])
    os.chdir("../output")
    export_csv(data_formated, filename)
    raw_file.close()
    os.chdir("../input")
    return meta_extracted, data_formated, data_extracted["reading"] # data_formated is the array form

if __name__ == "__main__":
    filelist = listfiles.getFileList()
    for filename in filelist:
        convert(filename)


